To run BeardCam, ensure you have the following:
	- Python 2.7.6
	- OpenCV 2.4.9 or greater and dependencies 
	- Numpy 1.8.1 (dependency of OpenCV)
	- Pygame 1.9.2 or greater and dependencies

Install Python first (if you have not already), and then install the 
above modules. Installation methods differ for Windows an Linux. Linux
systems use pip installer. Windows installers are located on the disc for
64 bit machines. You will have to download your own for 32 bit machines.

This program will not work with any version of Python 3 or greater.
