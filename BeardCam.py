import cv2.cv as cv
import cv2
import pygame
import time
import numpy
import sys

######constant list######
NO_COLORS = 256**3 #Number of possible colours in the RGB system
FRAME_SKIP = 1 #How often to detect for a new beard
DEBUG = False #Whether debug is on or not
CAMERA_INDEX = 0 #Which camera to use on the computer (0 is default camera)
HAAR_FOLDER = "C:\opencv\sources\data\haarcascades"
HAAR_FACE_PATH = HAAR_FOLDER +  "\haarcascade_frontalface_alt.xml"


######global variables######
width = 640
height = 480
beard_list = []
curr_beard = 0
histogram_equalization = False #If true, use histogram equalization on the frames
get_color = True #If true, get the person's hair colour.
test = False
def debug_print(*args):
    if DEBUG:
        print(args)

def fuse_color(*args):
    return min(255,sum(args)/len(args))

def split_color(integer):
    R = integer/(256*256)
    G = (integer%(256*256))/256
    B = (integer%(256*256))%256

    return [R,G,B]


def superposition(in_array, super_pos, start_x, start_y, color):
    super_array = pygame.PixelArray(super_pos)
    (w,h) = in_array.shape
    (ws,hs) = super_array.shape
    (r,g,b) = split_color(color)
    for i in xrange(ws):
        for j in xrange(hs):

            if (start_x + i <= w - 1 and start_y + j <= h - 1):
                fixed_color = super_array[i][j] % NO_COLORS #Makes sure the color is within bounds
                if fixed_color == 0:
                    
                    in_array[start_x + i][start_y + j] = pygame.Color(r,g,b)
                elif fixed_color == 255: #blue
                    in_array[start_x + i][start_y + j] = pygame.Color(fuse_color(r,127), fuse_color(g,127), fuse_color(b,127))
         
    

    return in_array

#Method from:
#http://www.calebmadrigal.com/facial-detection-opencv-python/
def detect_faces(image, cascade):
    faces = []
    detected = cv.HaarDetectObjects(image, cascade, storage, 1.2, 2, cv.CV_HAAR_DO_CANNY_PRUNING, (100,100))
    if detected:
        for (x,y,w,h),n in detected:
            faces.append((x,y,w,h))
    return faces


#Following three methods from:
#https://gist.github.com/jpanganiban/3844261
def surface_to_string(surface):
    """Convert a pygame surface into string"""
    return pygame.image.tostring(surface, 'RGB')
 
 
def pygame_to_cvimage(surface):
    """Convert a pygame surface into a cv image"""
    cv_image = cv.CreateImageHeader(surface.get_size(), cv.IPL_DEPTH_8U, 3)
    image_string = surface_to_string(surface)
    cv.SetData(cv_image, image_string)
    return cv_image


def cvimage_to_pygame(image):
    """Convert cvimage into a pygame image"""
    image_rgb = cv.CreateMat(image.height, image.width, cv.CV_8UC3)
    cv.CvtColor(image, image_rgb, cv.CV_BGR2RGB)
    return pygame.image.frombuffer(image.tostring(), cv.GetSize(image_rgb),
                                   "RGB")

###Read File###
f = open('config.txt')
lines = [i.strip() for i in f.readlines()]
curr_container = []
for line in lines:
    curr = line.split(':')
    if curr[0] == 'Width':
        print 'test'
        width = int(curr[1])
    elif curr[0] == 'Height':
        height = int(curr[1])
    elif curr[0] == '[Beard Files]':
        curr_container = beard_list
    else:
        curr_container.append(curr[0])
f.close()

debug_print (curr_container, beard_list)


storage = cv.CreateMemStorage()

cascade = cv.Load(HAAR_FACE_PATH)
faces = []

capture = cv2.VideoCapture(CAMERA_INDEX)

size=(width,height)
screen = pygame.display.set_mode(size)

num_beards = len(beard_list)
super_surface = (pygame.image.load(beard_list[curr_beard%num_beards]))

i = 0
###http://stackoverflow.com/questions/11528009/opencv-converting-from-numpy-to-iplimage-in-python
###converts numpy array to iplimage, seen below
while True:
    retval, image = capture.read() #capture.read returns a tuple

    if histogram_equalization:
        image = cv2.cvtColor(image, cv2.COLOR_RGB2YCR_CB)
        (channel_y, channel_cr, channel_cb) = cv2.split(image)
        channel_y = cv2.equalizeHist(channel_y)
       
        cv2.merge((channel_y, channel_cb, channel_cr), image)
        image = cv2.cvtColor(image, cv2.COLOR_YCR_CB2RGB)
    else:
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

    
    bitmap = cv.CreateImageHeader((image.shape[1], image.shape[0]), cv.IPL_DEPTH_8U, 3)
    cv.SetData(bitmap, image.tostring(), 
           image.dtype.itemsize * 3 * image.shape[1])    
    frame = bitmap


    if (i%FRAME_SKIP) == 0:
        faces = detect_faces(frame, cascade)

    if DEBUG:
        for (x,y,w,h) in faces:
            cv.Rectangle(frame, (x,y), (x+w,y+h), 255)
    newSurface = cvimage_to_pygame(frame)
    newArray = pygame.PixelArray(newSurface)

    color = 0

    
    for (x,y,w,h) in faces:
        debug_print (x,y,w,h)
        if get_color:
            color_x = int(x+w*.5)
            color_y = int(y-h*.1)
            if 0 <= color_x < width and 0 <= color_y < height:
                color = newArray[color_x][color_y]
                debug_print (split_color(color))

        curr_surface = pygame.transform.scale(super_surface, (int(w*1.25), h))
        newSurface = superposition(newArray, curr_surface, x-(int(w*1.125 - w)) , y+ int(h*.65),color).make_surface()
        
    del newArray

   
    for event in pygame.event.get():
         if event.type == pygame.QUIT:
             del(capture)
             pygame.quit()
             sys.exit()
         if event.type == pygame.KEYDOWN:
             if event.key == pygame.K_h:
                 histogram_equalization = not histogram_equalization
             if event.key == pygame.K_c:
                 get_color = not get_color
             if event.key == pygame.K_b:
                 curr_beard += 1
                 del super_surface
                 super_surface = (pygame.image.load(beard_list[curr_beard%num_beards]))
                 

     
    screen.blit(newSurface,(0,0))
    pygame.display.flip()
    i += 1

    
